<?php
/**
 * @file
 * Contains the PersonalizeAB decision agent definition.
 */

/**
 * PersonalizeAB decision agent definition.
 */
class PersonalizeAB extends PersonalizeAgentBase implements PersonalizeExplicitTargetingInterface {

  /**
   * {@inheritdoc}
   */
  public static function create($agent_data) {
    $status = personalize_agent_get_status($agent_data->machine_name);
    return new self($agent_data->machine_name, $agent_data->label, $agent_data->data, $status, !empty($agent_data->started) ? $agent_data->started : NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return 'personalize_ab';
  }

  /**
   * {@inheritdoc}
   */
  public function getAssets() {
    return array(
      'js' => array(
        drupal_get_path('module', 'personalize_ab') . '/js/personalize_ab.js' => array('type' => 'file', 'scope' => 'footer', 'defer' => TRUE),
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function explicitTargetingSupportMultiple() {
    return self::EXPLICIT_TARGETING_MULTIPLE_OR;
  }

}

