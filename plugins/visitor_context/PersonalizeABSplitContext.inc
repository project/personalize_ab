<?php
/**
 * @file
 * Contains the Personalize A/B Split context definition.
 */

/**
 * The Personalize A/B Split context definition.
 */
class PersonalizeABSplitContext extends PersonalizeContextBase implements PersonalizeContextInterface, PersonalizeLoggerAwareInterface {

  /**
   * {@inheritdoc}
   */
  public static function allowedFromAgent(StdClass $agent) {
    return $agent->plugin == 'personalize_ab';
  }

  /**
   * {@inheritdoc}
   */
  public function getAssets() {
    return array(
      'js' => array(
        drupal_get_path('module', 'personalize_ab') . '/js/personalize_ab_split_context.js' => array(),
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(PersonalizeAgentInterface $agent = NULL, $selected_context = array()) {
    return new self($agent, $selected_context);
  }

  /**
   * {@inheritdoc}
   */
  public static function getOptions() {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues($limit = FALSE) {
    $values = range(5, 100, 5);
    return array(
      'personalize_ab_split' => array(
        'friendly name' => t('A/B test weight'),
        'value type' => 'predefined',
        'values' => array_combine($values, preg_filter('/$/', ':1', $values)),
        'operator' => 'percent',
      ),
    );
  }

}