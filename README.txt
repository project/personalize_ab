INTRODUCTION
------------
This module extends the functionality of the Personalize module, adding a 
decision agent for A/B testing that isn't dependent on any Third Party calls or 
services. It provides a personalization agent that leverages the Executors of 
Personalize but does straight A/B testing on two or more Variants. Editors can 
optionally specify more complicated splits that aren't even distributions.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/personalize_ab

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2396189


REQUIREMENTS
------------
This module requires the following module(s):
 * Personalize (https://www.drupal.org/project/panels)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 MAINTAINERS
-----------
Current maintainers:
 * Michael DeWolf (mrmikedewolf) - https://drupal.org/user/2679073
