/**
 * @file
 * Personalize Split functionality.
 */

(function ($) {

  Drupal.personalize = Drupal.personalize || {};
  Drupal.personalize.visitor_context = Drupal.personalize.visitor_context || {};
  Drupal.personalize.visitor_context.personalize_ab_split_context = {
    'getContext': function(enabled) {
      // This method is a stub. It makes far more sense to determine the visitor
      // bucket in the agent since targeting is not always in use
      return {};
    }
  };

})(jQuery);