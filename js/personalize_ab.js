/**
 * @file
 * Frontend Personalize AB decision agent functionality.
 */

(function($) {

  Drupal.personalize = Drupal.personalize || {};
  Drupal.personalize.agents = Drupal.personalize.agents || Object.create(Drupal.personalize.agents.default_agent);
  Drupal.personalize.agents.personalize_ab = {
    'getDecisionsForPoint': function(agent_name, visitor_context, choices, decision_point, fallbacks, callback) {
      const settings = Drupal.settings.personalize.option_sets;
      if (!settings.hasOwnProperty(decision_point) || !choices.hasOwnProperty(decision_point)) {
        return;
      }

      let optionSet = settings[decision_point], options = choices[decision_point], decisions = {};
      if (optionSet.hasOwnProperty('targeting')) {
        for (let i in optionSet.targeting) {
          let targeting = optionSet.targeting[i];
          if (!targeting.hasOwnProperty('option_id') || !targeting.hasOwnProperty('targeting_features')) {
            continue;
          }

          for (rule in targeting.targeting_rules) {
            if (!targeting.targeting_rules[rule].hasOwnProperty('match')) {
              continue;
            }

            let target = targeting.targeting_rules[rule].match;
            // Since choices already has one of this option in it we weight it 
            // by doing choices - 1.
            options = options.concat(Array.apply(null, {length: target - 1}).fill(targeting.option_id));
          }
        }
      }

      // Getting a random array item in our (potentially) adjusted array.
      // @see https://www.w3resource.com/javascript-exercises/javascript-array-exercise-35.php
      let bucket = Math.floor((Math.random() * options.length));
      decisions[decision_point] = options[bucket];
      callback(decisions);
    }
  };

})(jQuery);
